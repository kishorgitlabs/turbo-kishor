import { ConfigService } from 'src/app/services/config/config.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss'],
})
export class ForgotComponent implements OnInit {

getForGotAlert:string;

  forGotForm:FormGroup;
  email:string;


  constructor(
    private config:ConfigService,
    private navparams:NavParams
  ) { }

  ngOnInit() {

    this.getForGotAlert=this.navparams.data.forgot;
    this.forGotForm=new FormGroup({
      email:new FormControl('',Validators.compose([
        Validators.required
      ]))
    });
  }

  async getPassword(){

this.config.loader('Loading...');

const values={
  
}



  }

}
