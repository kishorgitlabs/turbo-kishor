import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-viewpdf',
  templateUrl: './viewpdf.component.html',
  styleUrls: ['./viewpdf.component.scss'],
})
export class ViewpdfComponent implements OnInit {

  url;
  title = 'PDF';
  isShow: boolean = true;
  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
  ) { }

  ngOnInit() {
    // console.log(this.navParams.data.url);
    // this.url = 'https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf';
    this.title = this.navParams.data.title;
    // this.url = `https://cors-anywhere.herokuapp.com/${this.navParams.data.url}`;
    this.url = `https://addison-cors.herokuapp.com/${this.navParams.data.url}`;
    setTimeout(() => {
      this.isShow = false;
    }, 10000);
  }

  onProgress(e) {
    console.log('on-progress', e);
    this.isShow = false;
  }

  close() {
    this.modalCtrl.dismiss();
  }


}
