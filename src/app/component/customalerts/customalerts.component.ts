import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-customalerts',
  templateUrl: './customalerts.component.html',
  styleUrls: ['./customalerts.component.scss'],
})
export class CustomalertsComponent implements OnInit {

  alertName: string;



  constructor(
    private navParams: NavParams,
    private modalctrl:ModalController,
    private router:Router
  ) { }

  ngOnInit() {
    this.alertName = this.navParams.data.alertName;
    
  }
  async cancel(){
this.modalctrl.dismiss();
  }

  async okay(alertName){

    if(alertName === 'Please Wait for Admin Approval'){

      this.router.navigate(['/dashboard']);
    this.modalctrl.dismiss();
    }
    else if(alertName === 'Your device is not matched .Please contact turbo team' && alertName === 'No Internet Connection'){
 
      navigator['app'].exitApp();
      this.modalctrl.dismiss();
    }
    else if(alertName === 'Login Successfully'){
      this.modalctrl.dismiss();
      this.router.navigate(['/dashboard']);
    }
    

  }

}
