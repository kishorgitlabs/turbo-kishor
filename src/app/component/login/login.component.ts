import { ConfigService } from './../../services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ForgotComponent } from '../forgot/forgot.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  
  constructor(
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder,
    private config: ConfigService,
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  get _v() {
    return this.loginForm.value;
  }
  ngOnInit() { }

  close() {
    this.modalCtrl.dismiss();
  }

  async forGotPassword(){
   this.config.forGotPassword('AlertName');
  }

  async onFormSubmit(value){

    if(value.username === ''){
this.config.toastFn('Enter your user name');
    }
    else if(value.password === ''){
      this.config.toastFn('Enter your password');
    }
    else{
this.checkUserLogin(value);
    }
  }
  async checkUserLogin(value){

    const values={
      "username":value.username,
        "password":value.password
      
    }

    this.config.loader('Loading...');
    this.config.postData('api/mobile/userlogin',values).subscribe((res)=>{

      let response :any=res;

      if(response.result === true ){

        this.config.loaderDismiss();
        this.config.companyAlert('Login Successfully');
        // this.modalCtrl.dismiss();
      
      }
      else if(response.result === false ){
        this.config.loaderDismiss();
        this.config.companyAlert('Login Failed.Please Enter Correct User Name or Password'); 
       
      }
      else{
        this.config.loaderDismiss();
        this.config.companyAlert('Server responding error.Please contact admin .');
      }

    });
     

  }


  }

