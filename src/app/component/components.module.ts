import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ImagemodalComponent } from './imagemodal/imagemodal.component';
import { IonicModule } from '@ionic/angular';
import { ViewpdfComponent } from './viewpdf/viewpdf.component';
import { PdfViewerModule } from 'ng2-pdf-viewer'; // <- import PdfViewerModule
import { ForgotComponent } from './forgot/forgot.component';
import { CustomalertsComponent } from './customalerts/customalerts.component';
@NgModule({
  declarations: [
    HeaderComponent,
    ImagemodalComponent,
    ViewpdfComponent,
    LoginComponent,
    ForgotComponent,
    CustomalertsComponent
  ],
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PdfViewerModule,
  ],
  exports: [
    HeaderComponent,
    ImagemodalComponent,
    ViewpdfComponent,
    LoginComponent,
    ForgotComponent,
    CustomalertsComponent
  ]
})
export class ComponentsModule { }
