import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, IonSlides, NavParams } from '@ionic/angular';
@Component({
  selector: 'app-imagemodal',
  templateUrl: './imagemodal.component.html',
  styleUrls: ['./imagemodal.component.scss'],
})
export class ImagemodalComponent implements OnInit {

  @ViewChild(IonSlides) slides: IonSlides;
  // @Input('img') img: any;

  sliderOpts = {
    zoom: true
  };
  imgs;
  constructor(
    private navParams: NavParams,
    private modalController: ModalController) { }

  ngOnInit() {
    this.imgs = this.navParams.data.img;
  }

  ionViewDidEnter() {
    this.slides.update();
  }

  async zoom(zoomIn: boolean) {
    const slider = await this.slides.getSwiper();
    const zoom = slider.zoom;
    zoomIn ? zoom.in() : zoom.out();
  }

  close() {
    this.modalController.dismiss();
  }
}
