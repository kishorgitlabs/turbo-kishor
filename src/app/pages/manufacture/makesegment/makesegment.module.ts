import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MakesegmentPageRoutingModule } from './makesegment-routing.module';
import { MakesegmentPage } from './makesegment.page';
import { ComponentsModule } from 'src/app/component/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    MakesegmentPageRoutingModule
  ],
  declarations: [MakesegmentPage]
})
export class MakesegmentPageModule { }
