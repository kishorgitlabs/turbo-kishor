import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {

  getMakeName:string;
  getSegmentName :string;
  getSubSegmentName:string;
  makeApplicationtJson=[];

  constructor(
    private activateroute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {


    this.activateroute.queryParams.subscribe((res)=>{

      const obj=JSON.parse(res.makeSubSegmentObj);
      this.getMakeName=obj.makeName;
      this.getSegmentName=obj.segmentName;
      this.getSubSegmentName=obj.subSegment;

    });
  
    this.getMakeApplication();
  }


  async getMakeApplication(){

    const values= {
          "oem_name":this.getMakeName,
              "segment":this.getSegmentName,
              "sub_segment":this.getSubSegmentName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getMakeApplication',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.makeApplicationtJson=response.data;

      }
      else if(response.result === false){
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');
      }
      else{
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
      }
   },err=>{
     this.config.loaderDismiss();
     console.log(err);
   });

 }

 async gotoPartDetails(a){

  const values={
    oldPartNo:a.oldpartno,
    oempartno:a.oempartno,
    engine:a.engine,
    application:a.application,
    makename:this.getMakeName,
    newPartNo:a.newpartno
  }

  let navigation:NavigationExtras={
    queryParams:{
      apllicationObj:JSON.stringify(values)
    }
  };
this.router.navigate([`/partdetails`],navigation);
  
 }

}
