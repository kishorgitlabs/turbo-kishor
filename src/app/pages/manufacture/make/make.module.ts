import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MakePageRoutingModule } from './make-routing.module';
import { MakePage } from './make.page';
import { ComponentsModule } from 'src/app/component/components.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    Ng2SearchPipeModule,
    MakePageRoutingModule
  ],
  declarations: [MakePage]
})
export class MakePageModule { }
