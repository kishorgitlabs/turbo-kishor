import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-make',
  templateUrl: './make.page.html',
  styleUrls: ['./make.page.scss'],
})
export class MakePage implements OnInit {

  ngSearchTerm;
   makeListJson:any =[];

  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
   this.getMakeList();
  }

  async getMakeList(){

       this.config.loader('Loading...');
        this.config.getData('api/mobile/getMake').subscribe((res)=>{
          console.log("Subscription Success");
           const response:any =res;
           if(response.result === true){
             console.log("Respnse Success");
            this.config.loaderDismiss();
            this.makeListJson=response.data;
  

           }
           else if(response.result === false){
            this.config.loaderDismiss();
            this.config.msgAlertFn('No Data Found');
          }
           else{
             this.config.loaderDismiss();
             this.config.msgAlertFn('No Data Found');
           }
        },err=>{
         this.config.loaderDismiss();
        });

  }

  async gotoMakeSegment(obj){

    const values={
      make:obj
    }
    const navigationExtras:NavigationExtras={
      queryParams:{
        makeObj:JSON.stringify(values)
      }
    };
    this.router.navigate([`/makesegment`],navigationExtras);
  }
}
