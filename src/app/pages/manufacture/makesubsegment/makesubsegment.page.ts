import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-makesubsegment',
  templateUrl: './makesubsegment.page.html',
  styleUrls: ['./makesubsegment.page.scss'],
})
export class MakesubsegmentPage implements OnInit {

  getMakeName:string;
  getSegmentName :string;
  subSegmentListJson=[];

  constructor(
    private activateroute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
 

    this.activateroute.queryParams.subscribe((res)=>{

      const obj =JSON.parse(res.makeSegmentObj);
      this.getMakeName=obj.makename;
      this.getSegmentName=obj.segmentName;

    });

   

    this.getsubSegmentDetails();
  }


  async getsubSegmentDetails(){

    const values= {
     "oem_name":this.getMakeName,
     "segment":this.getSegmentName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getMakeSubSegment',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.subSegmentListJson=response.data;

      }
      else if(response.result === false){
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');
      }
      else{
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
      }
   },err=>{
     this.config.loaderDismiss();
     console.log(err);
   });

 }

 async gettoApplication(subsegment){


  const values={
    makeName:this.getMakeName,
    segmentName:this.getSegmentName,
    subSegment:subsegment
  }

  let navigation:NavigationExtras={
    queryParams:{
      makeSubSegmentObj:JSON.stringify(values)
    }
  }
this.router.navigate([`/application`],navigation);
  
 }

}
