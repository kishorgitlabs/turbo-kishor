import { ComponentsModule } from 'src/app/component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MakesubsegmentPageRoutingModule } from './makesubsegment-routing.module';
import { MakesubsegmentPage } from './makesubsegment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    MakesubsegmentPageRoutingModule
  ],
  declarations: [MakesubsegmentPage]
})
export class MakesubsegmentPageModule { }
