import { ComponentsModule } from 'src/app/component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PartdetailsPageRoutingModule } from './partdetails-routing.module';
import { PartdetailsPage } from './partdetails.page';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    ComponentsModule,
    PartdetailsPageRoutingModule
  ],
  declarations: [PartdetailsPage]
})
export class PartdetailsPageModule { }
