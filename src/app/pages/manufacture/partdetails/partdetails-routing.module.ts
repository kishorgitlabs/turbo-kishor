import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartdetailsPage } from './partdetails.page';

const routes: Routes = [
  {
    path: '',
    component: PartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PartdetailsPageRoutingModule {}
