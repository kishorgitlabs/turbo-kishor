import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ImagemodalComponent } from 'src/app/component/imagemodal/imagemodal.component';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-partdetails',
  templateUrl: './partdetails.page.html',
  styleUrls: ['./partdetails.page.scss'],
})
export class PartdetailsPage implements OnInit {

  type: string = 'Specification';

  applicationPartDetailsJson =[];
  getOldPartNo:string;
  getOemPartNo:string;
  getEngineName:string;
  getApplication:string;
  getMakeName:string;
  getNewPartNo:string;


  constructor(
    private modalCtrl: ModalController,
    private activateroute:ActivatedRoute,
    private config:ConfigService
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res)=>{

      const obj =JSON.parse(res.apllicationObj);
      this.getOldPartNo=obj.oldPartNo;
      this.getOemPartNo=obj.oempartno;
      this.getEngineName=obj.engine;
      this.getApplication=obj.application;
      this.getMakeName=obj.makename;
       this.getNewPartNo=obj.newPartNo;
    })

    this.getMakeApplicationPartDetails();

  }

  async openPreview() {
    const imgURL = "../../../../assets/CentralHousing.jpg";
    const modal = await this.modalCtrl.create({
      component: ImagemodalComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        img: imgURL
      }
    });
    modal.present();
  }

  async getMakeApplicationPartDetails(){

    const values= {
               "tca_oldpartno":this.getOldPartNo,
               "oem_partnumber":this.getOemPartNo,
                "engine":this.getEngineName,
                "application":this.getApplication
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getMakeApplicationParDetails',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss(); 
       this.applicationPartDetailsJson=response.data;
      }
      else if(response.result === false){
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');
        alert('false');

      }
      else{
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
       alert('else');
      }
   },err=>{
     this.config.loaderDismiss();
     console.log(err);
   });

 }


}
