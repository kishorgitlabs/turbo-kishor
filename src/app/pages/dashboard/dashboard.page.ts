import { LoginComponent } from './../../component/login/login.component';
import { Component, OnInit } from '@angular/core';
import { MenuController, Platform, AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  slidesOptions = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };
  constructor(
    private menuCtrl: MenuController,
    private platform: Platform,
    private router: Router,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private device: Device
  ) {
    this.platform.backButton.subscribe(async () => {
      if (this.router.url === '/dashboard') {
        this.exitFunction();
      }
    });
  }

  ngOnInit() {
    console.log('Device UUID is: ' + this.device.uuid);
    console.log('Device version is: ' + this.device.version);
    console.log('Device manufacturer is: ' + this.device.manufacturer);
    console.log('Device model is: ' + this.device.model);
    console.log('Device platform is: ' + this.device.platform);
    console.log('Device serial is: ' + this.device.serial);

    setTimeout(() => {
      console.log('Device UUID is: ' + this.device.uuid);
      console.log('Device version is: ' + this.device.version);
      console.log('Device manufacturer is: ' + this.device.manufacturer);
      console.log('Device model is: ' + this.device.model);
      console.log('Device platform is: ' + this.device.platform);
      console.log('Device serial is: ' + this.device.serial);
    }, 2000);

    this.menuCtrl.enable(true);
  }

  async exitFunction() {
    const alert = await this.alertCtrl.create({
      header: 'Exit App ?',
      message: 'Do you want to exit the app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Exit',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });
    await alert.present();
  }

  async chkLogin(pageurl) {
    console.log(localStorage.getItem('lsLogin'));
    if (localStorage.getItem('lsLogin') === null) {
      const modal = await this.modalCtrl.create({
        component: LoginComponent,
      });
      modal.present();
    } else {
      this.router.navigateByUrl(`/${pageurl}`);
    }
  }

}
