import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { OthersPageRoutingModule } from './others-routing.module';
import { OthersPage } from './others.page';
import { ComponentsModule } from 'src/app/component/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    OthersPageRoutingModule
  ],
  declarations: [OthersPage]
})
export class OthersPageModule { }
