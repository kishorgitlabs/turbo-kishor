import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.page.html',
  styleUrls: ['./pdf.page.scss'],
})
export class PdfPage implements OnInit {
  pdfArr = [];
  constructor() { }

  ngOnInit() {
    this.pdfArr = [
      'Price List',
      'Catalogue',
    ];
  }

}
