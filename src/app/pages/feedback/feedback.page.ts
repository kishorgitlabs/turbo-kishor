import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

  feedbackForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.feedbackForm = new FormGroup({
      subject: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required])
    });
  }
  onFormSubmit() {
    if (!this.feedbackForm.valid) {
      this.feedbackForm.markAllAsTouched();
      return;
    }
    console.log(this._v)
  }
  get _v() {
    return this.feedbackForm.value;
  }

  ngOnInit() {
  }

}
