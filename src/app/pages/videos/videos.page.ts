import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.page.html',
  styleUrls: ['./videos.page.scss'],
})
export class VideosPage implements OnInit {
  ngSearchTerm: string;
  videosArr = [];
  constructor() { }

  ngOnInit() {
    this.videosArr = [
      'Auto After Market Videos',
      'Catalogues',
      'Training Videos'
    ];
  }

}
