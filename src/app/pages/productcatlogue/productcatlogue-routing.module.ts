import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductcatloguePage } from './productcatlogue.page';

const routes: Routes = [
  {
    path: '',
    component: ProductcatloguePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductcatloguePageRoutingModule {}
