import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ProductcatloguePageRoutingModule } from './productcatlogue-routing.module';
import { ComponentsModule } from '../../component/components.module';
import { ProductcatloguePage } from './productcatlogue.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductcatloguePageRoutingModule,
    ComponentsModule,
    Ng2SearchPipeModule
  ],
  declarations: [ProductcatloguePage]
})
export class ProductcatloguePageModule {}
