import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ViewpdfComponent } from 'src/app/component/viewpdf/viewpdf.component';

@Component({
  selector: 'app-pdfdetails',
  templateUrl: './pdfdetails.page.html',
  styleUrls: ['./pdfdetails.page.scss'],
})
export class PdfdetailsPage implements OnInit {

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }
  async openPDFFn() {
    const pdfURL = `https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf`;
    const modal = await this.modalCtrl.create({
      component: ViewpdfComponent,
      // cssClass: 'custom-modal',
      componentProps: {
        url: pdfURL,
        title: 'PDF'
      }
    });
    modal.onWillDismiss().then(() => {
      // this.fab.nativeElement.classList.remove('animated', 'bounceOutLeft')
      // this.animateCSS('bounceInLeft');
    });
    modal.present();
  }
}
