import { Component, OnInit } from '@angular/core';
 import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { ConfigService } from '../../services/config/config.service';
@Component({
  selector: 'app-videosdetails',
  templateUrl: './videosdetails.page.html',
  styleUrls: ['./videosdetails.page.scss'],
})
export class VideosdetailsPage implements OnInit {

  constructor(
    private iab: InAppBrowser,
    public config: ConfigService
  ) { }

  ngOnInit() {
  }

  demoVideo() {
    const options: InAppBrowserOptions = {
      location: 'no',
      clearcache: 'yes',
      zoom: 'yes',
      toolbar: 'no',
      closebuttoncaption: 'back'
    };
    const browser: any = this.iab.create('https://www.youtube.com/', '_blank', options);
  }


}
