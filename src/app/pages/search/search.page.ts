import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  mode;

  constructor(
    private activateroute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.mode = this.activateroute.snapshot.paramMap.get('mode');
  }

}
