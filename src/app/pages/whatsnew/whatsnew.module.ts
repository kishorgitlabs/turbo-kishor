import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { WhatsnewPageRoutingModule } from './whatsnew-routing.module';
import { WhatsnewPage } from './whatsnew.page';
import { ComponentsModule } from '../../component/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    WhatsnewPageRoutingModule
  ],
  declarations: [WhatsnewPage]
})
export class WhatsnewPageModule { }
