import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchbypartnoPage } from './searchbypartno.page';

const routes: Routes = [
  {
    path: '',
    component: SearchbypartnoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchbypartnoPageRoutingModule {}
