import { ComponentsModule } from 'src/app/component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SearchbypartnoPageRoutingModule } from './searchbypartno-routing.module';
import { SearchbypartnoPage } from './searchbypartno.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SearchbypartnoPageRoutingModule
  ],
  declarations: [SearchbypartnoPage]
})
export class SearchbypartnoPageModule { }
