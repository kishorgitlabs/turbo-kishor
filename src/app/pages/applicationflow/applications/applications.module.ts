import { ComponentsModule } from './../../../component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ApplicationsPageRoutingModule } from './applications-routing.module';
import { ApplicationsPage } from './applications.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    ComponentsModule,
    ApplicationsPageRoutingModule
  ],
  declarations: [ApplicationsPage]
})
export class ApplicationsPageModule { }
