import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.page.html',
  styleUrls: ['./applications.page.scss'],
})
export class ApplicationsPage implements OnInit {

  ngSearchTerm;
  applicationJson=[];
  
  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getApplication();
  }

  
  async getApplication(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getApplication').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.applicationJson=response.data;
        }
        else if(response.result === false){
         this.config.loaderDismiss();
         this.config.msgAlertFn('No Data Found');
       }
        else{
          this.config.loaderDismiss();
          this.config.msgAlertFn('No Data Found');
        }
     },err=>{
      this.config.loaderDismiss();
     });

}

async gotoEngineSpecs(application){

const values={
  applicationName:application
}
let navigation:NavigationExtras={
  queryParams:{
    applicatonObj:JSON.stringify(values)
  }
};
this.router.navigate(['/enginespecs'],navigation);

}

}
