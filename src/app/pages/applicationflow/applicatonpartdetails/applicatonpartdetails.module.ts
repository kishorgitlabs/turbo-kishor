import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplicatonpartdetailsPageRoutingModule } from './applicatonpartdetails-routing.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { ApplicatonpartdetailsPage } from './applicatonpartdetails.page';
import { ComponentsModule } from 'src/app/component/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ApplicatonpartdetailsPageRoutingModule,
    MatExpansionModule,
    ComponentsModule
  ],
  declarations: [ApplicatonpartdetailsPage]
})
export class ApplicatonpartdetailsPageModule {}
