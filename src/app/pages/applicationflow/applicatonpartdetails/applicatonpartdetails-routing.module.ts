import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicatonpartdetailsPage } from './applicatonpartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ApplicatonpartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicatonpartdetailsPageRoutingModule {}
