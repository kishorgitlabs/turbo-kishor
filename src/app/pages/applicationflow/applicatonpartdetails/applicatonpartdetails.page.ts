import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-applicatonpartdetails',
  templateUrl: './applicatonpartdetails.page.html',
  styleUrls: ['./applicatonpartdetails.page.scss'],
})
export class ApplicatonpartdetailsPage implements OnInit {

  type: string = 'Specification';

  oldPartNo:string;
  newPartNo:string;
  oemPartNo:string;
  engine:string;
  application:string;

  applicationListJson=[];
  constructor(
    private activatedRoute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(params => {
      const obj = JSON.parse(params.engineObj);
      this.oldPartNo=obj.oldPartNo,
      this.newPartNo=obj.newPartNo,
      this.oemPartNo=obj.oemPartNo,
      this.engine=obj.engine,
      this.application=obj.applicationName

   });
this.getApplicationPartDeatils();
  }

  async getApplicationPartDeatils(){

    const values= {
      "tca_oldpartno": this.oldPartNo,
      "tca_newpartno": this.newPartNo,
      "oem_partnumber": this.oemPartNo,
      "engine":this.engine,
      "application": this.application
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getApplicationPartDetails',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.applicationListJson=response.data;
      
      }
      else if(response.result === false){
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
     }
      else{
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
      }
   },err=>{
     this.config.loaderDismiss();
     console.log(err);
   });

 }

}
