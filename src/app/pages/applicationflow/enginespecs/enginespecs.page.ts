import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-enginespecs',
  templateUrl: './enginespecs.page.html',
  styleUrls: ['./enginespecs.page.scss'],
})
export class EnginespecsPage implements OnInit {


  getApplicationName :string;
  applicationListJson=[];

  constructor(
    private activatedRoute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
       const obj = JSON.parse(params.applicatonObj);
       this.getApplicationName=obj.applicationName
    });
this.getEngineSpecs();
  }


  async getEngineSpecs(){

    const values= {
     "application":this.getApplicationName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getApplicationPart',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.applicationListJson=response.data;

      }
      else if(response.result === false){
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
     }
      else{
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
      }
   },err=>{
     this.config.loaderDismiss();
     console.log(err);
   });

 }

 async gotoSegmentPartDetails(applicationdetails){

  const values={
    oldPartNo:applicationdetails.oldpartno,
    newPartNo:applicationdetails.newpartno,
    oemPartNo:applicationdetails.oempartno,
    engine:applicationdetails.engine,
    applicationName:this.getApplicationName

  }
  let navigation:NavigationExtras={
    queryParams:{
      engineObj:JSON.stringify(values)
    }
  };
  this.router.navigate(['/applicatonpartdetails'],navigation)

 }

}
