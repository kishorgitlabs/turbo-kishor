import { ComponentsModule } from 'src/app/component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PricelistPageRoutingModule } from './pricelist-routing.module';
import { PricelistPage } from './pricelist.page';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    NgSelectModule,
    PricelistPageRoutingModule
  ],
  declarations: [PricelistPage]
})
export class PricelistPageModule { }
