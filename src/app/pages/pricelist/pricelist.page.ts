import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-pricelist',
  templateUrl: './pricelist.page.html',
  styleUrls: ['./pricelist.page.scss'],
})
export class PricelistPage implements OnInit {

  makeNameJson=[];
  segmentJson=[];
 ngModel;
 makeName;
 segmentName;

  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {

   this.getMakeList(); 
  }

  async getMakeList(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getMake').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.makeNameJson=response.data;
         
        }
        else if(response.result === false){
         this.config.loaderDismiss();
         this.config.msgAlertFn('No Data Found');
       }
        else{
          this.config.loaderDismiss();
          this.config.msgAlertFn('No Data Found');
        }
     },err=>{
      this.config.loaderDismiss();
     });

}

async getSegmentDetails(obj){

  if(this.makeName === null){
    this.segmentName=[];
  }

  const values= {
   "oem_name":obj.make
  }
 this.config.loader('Loading...');
 this.config.postData('api/mobile/getMakeSegment',values).subscribe((res)=>{

    const response:any =res;

    if(response.result === true){
     this.config.loaderDismiss();
     this.segmentJson=response.data;

    }
    else if(response.result === false){
     this.config.loaderDismiss();
     this.config.msgAlertFn('No Data Found');
   }
    else{
     this.config.loaderDismiss();
     this.config.msgAlertFn('No Data Found');
    }
 },err=>{
   this.config.loaderDismiss();
   console.log(err);
 });

}
}
