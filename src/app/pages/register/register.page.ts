import { Component, OnInit } from '@angular/core';
import { MenuController, AlertController, ModalController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';
import { ConfigService } from 'src/app/services/config/config.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


    turboRegisterForm:FormGroup;
    usertype:string;
    name:string;
    mobileno:string;
    mail:string;
    state:string;
    city:string;
    pincode:string;
    selectusertype;
    selectstate;
  public  otpChangeText:string='Generate OTP';

    enableOtp:boolean=false;
    devicename: any;
    devicemodel: any;
    devicetype: any;


  constructor(
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController,
    private route: Router,
    private alertController: AlertController,
    private device: Device,
    private config: ConfigService,
  ) {  

  }

  ngOnInit() {

    setTimeout(() => {
      this.devicename = this.device.manufacturer;
    this.devicemodel = this.device.model;
    this.devicetype = this.device.platform;
    }, 2000);

    this.turboRegisterForm=new FormGroup({
    
       usertype:new FormControl('',Validators.compose([Validators.required])),
       name:new FormControl('',Validators.compose([Validators.required])),

       mobileno:new FormControl('',Validators.compose([
         Validators.required,
         Validators.minLength(10),
         Validators.maxLength(10)
       ])),
       mail:new FormControl('',Validators.compose([
         Validators.required,
         Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])
       ),

       state:new FormControl('',Validators.compose([Validators.required])),
       city:new FormControl('',Validators.compose([Validators.required])),
       pincode:new FormControl('',Validators.compose([
         Validators.required,
         Validators.minLength(6),
         Validators.maxLength(6)
       ]))
 

    });
    
    this.menuCtrl.enable(false);
    this.selectusertype = [
      'ASC',
      'Distributor',
      'TEL Employee',
      'End Customer'
    ];
    this.selectstate = [
      'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar',
      'Chhattisgarh', 'Goa', 'Gujarat', 'Haryana',
      'Himachal Pradesh', 'Jharkhand', 'Karnataka', 'Kerala',
      'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya',
      'Mizoram', 'Nagaland', 'Odisha', 'Punjab',
      'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana',
      'Tripura', '	Uttar Pradesh', 'Uttarakhand', 'West Bengal'
    ];
  }

  clear() {
    this.turboRegisterForm.reset();
  }

async generateotp(){

    this.enableOtp =true;
    this.otpChangeText='Resend OTP'
    

}

  onFormSubmit(value) {

    const registerValues = {
      "username": value.name,
      "mobileno": value.mobileno,
      "devicemodel": this.devicemodel,
      "devicename": this.devicename,
      "devicetype": this.devicetype,
      "email": value.email,
      "state": value.state,
      "city": value.city,
      "usertype": value.usertype
    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/userRegistration', registerValues).subscribe((res) => {
      let response: any = res;
      if (response.result === true &&  response.msg === 'User Registration Successfully ...') {
        this.config.loaderDismiss();
        console.log(this._v);
        localStorage.setItem('lsuserdetails', this._v);
        localStorage.setItem('registertoken', 'true'); 
        this.turboRegisterForm.reset();
         this.config.companyAlert('Please Wait for Admin Approval');
         alert('new')
      }
      else if(response.result === true &&  response.msg === 'User already exists ...'){
        this.config.loaderDismiss();
        localStorage.setItem('lsuserdetails', this._v);
        localStorage.setItem('registertoken', 'true');
        this.turboRegisterForm.reset();
        this.config.companyAlert('Please Wait for Admin Approval');
        alert('old');
      }
      else if(response.result === false &&  response.msg === "User doesn't match ..."){
        this.config.loaderDismiss();
        this.turboRegisterForm.reset(); 
        this.config.companyAlert('Your device is not matched .Please contact turbo team');
        alert('not')
      }
      else{
        this.config.loaderDismiss();     
      }
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
  
    })
  }

  get _v() {
    return this.turboRegisterForm.value;
  }


}
