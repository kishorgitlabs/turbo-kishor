import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RegisterPageRoutingModule } from './register-routing.module';
import { RegisterPage } from './register.page';
import { ComponentsModule } from '../../component/components.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOtpInputModule } from  'ng-otp-input';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    NgSelectModule,
    ComponentsModule,
    RegisterPageRoutingModule,
    NgOtpInputModule
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule { }
