import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkPageRoutingModule } from './network-routing.module';
import { NetworkPage } from './network.page';
import { ComponentsModule } from '../../component/components.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgSelectModule,
    ComponentsModule,
    NetworkPageRoutingModule
  ],
  declarations: [NetworkPage]
})
export class NetworkPageModule {}
