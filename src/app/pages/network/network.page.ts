import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-network',
  templateUrl: './network.page.html',
  styleUrls: ['./network.page.scss'],
})
export class NetworkPage implements OnInit {

  type: string;
  states = [
    'Tamilnadu',
    'Kerala',
    'Karnataka',
    'Delhi',
    'Bihar',
  ];
  city = [
    'Chennai',
    'Trichy',
    'Madurai',
    'Coimbatore',
    'Salem'
  ];
  constructor() { }
  ngOnInit() {
    this.type = 'Distributor';
  }

}
