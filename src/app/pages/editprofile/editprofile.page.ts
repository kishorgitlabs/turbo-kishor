import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {

  editProfileForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.editProfileForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [Validators.required])
    });
  }
  onFormSubmit() {
    if (!this.editProfileForm.valid) {
      this.editProfileForm.markAllAsTouched();
      return;
    }
    console.log(this._v)
  }
  get _v() {
    return this.editProfileForm.value;
  }

  ngOnInit() {
  }

}
