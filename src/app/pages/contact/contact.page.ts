import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  type: string;
  states = [
    'Tamilnadu',
    'Kerala',
    'Karnataka',
    'Delhi',
    'Bihar',
  ];
  city = [
    'Chennai',
    'Trichy',
    'Madurai',
    'Coimbatore',
    'Salem'
  ];
  ContactTypeArr = [
    'Enquiry',
    'Feedback'
  ];
  feedbackForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.feedbackForm = new FormGroup({
      contactType: new FormControl(null, [Validators.required]),
      subject: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    this.type = 'SalesTeam';
  }

  onFormSubmit() {
    if (!this.feedbackForm.valid) {
      this.feedbackForm.markAllAsTouched();
      return;
    }
    console.log(this._v);
  }
  get _v() {
    return this.feedbackForm.value;
  }

}
