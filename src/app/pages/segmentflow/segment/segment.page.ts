import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-segment',
  templateUrl: './segment.page.html',
  styleUrls: ['./segment.page.scss'],
})
export class SegmentPage implements OnInit {

  segmentListJson:any =[];

  constructor(
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.getSegment();
  }
  async getSegment(){

    this.config.loader('Loading...');
     this.config.getData('api/mobile/getSegment').subscribe((res)=>{
       console.log("Subscription Success");
        const response:any =res;
        if(response.result === true){
          console.log("Respnse Success");
         this.config.loaderDismiss();
         this.segmentListJson=response.data;


        }
        else if(response.result === false){
         this.config.loaderDismiss();
         this.config.msgAlertFn('No Data Found');
       }
        else{
          this.config.loaderDismiss();
          this.config.msgAlertFn('No Data Found');
        }
     },err=>{
      this.config.loaderDismiss();
     });

}

async gotoSubSegment(segment){

  let navigation:NavigationExtras={
    queryParams:{
      segmentObj:segment
    }
  }
  this.router.navigate([`/segmentsubsegment`],navigation)
}
}
