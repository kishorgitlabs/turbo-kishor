import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-segmentsubsegment',
  templateUrl: './segmentsubsegment.page.html',
  styleUrls: ['./segmentsubsegment.page.scss'],
})
export class SegmentsubsegmentPage implements OnInit {

  getSegmentName:string;
  getSubSegmentJson=[];

  constructor(
    private activateroute:ActivatedRoute,
    private config:ConfigService,
    private router:Router
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res)=>{
      this.getSegmentName=res.segmentObj;

    });
this.getSegmentDetails();
  }

  async getSegmentDetails(){

    const values= {
     "segment":this.getSegmentName
    }
   this.config.loader('Loading...');
   this.config.postData('api/mobile/getSubSegment',values).subscribe((res)=>{

      const response:any =res;

      if(response.result === true){
       this.config.loaderDismiss();
       this.getSubSegmentJson=response.data;
        
      }
      else if(response.result === false){
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
     }
      else{
       this.config.loaderDismiss();
       this.config.msgAlertFn('No Data Found');
      }
   },err=>{
     this.config.loaderDismiss();
     console.log(err);
   });

 }

 async gotoSegmentMake(subsegment){


  const values={
    segmentName:this.getSegmentName,
    subSegment:subsegment
  }
  let navigation:NavigationExtras={
    queryParams:{
      subSegmentObj:JSON.stringify(values)
    }
  };
  
  this.router.navigate([`/segmentmake`],navigation);

 }
}
