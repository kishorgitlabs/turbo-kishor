import { ComponentsModule } from './../../../component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SegmentsubsegmentPageRoutingModule } from './segmentsubsegment-routing.module';
import { SegmentsubsegmentPage } from './segmentsubsegment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SegmentsubsegmentPageRoutingModule
  ],
  declarations: [SegmentsubsegmentPage]
})
export class SegmentsubsegmentPageModule { }
