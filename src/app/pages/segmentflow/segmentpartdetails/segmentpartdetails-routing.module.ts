import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegmentpartdetailsPage } from './segmentpartdetails.page';

const routes: Routes = [
  {
    path: '',
    component: SegmentpartdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SegmentpartdetailsPageRoutingModule {}
