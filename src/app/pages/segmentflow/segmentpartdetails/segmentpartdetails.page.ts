import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ImagemodalComponent } from 'src/app/component/imagemodal/imagemodal.component';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-segmentpartdetails',
  templateUrl: './segmentpartdetails.page.html',
  styleUrls: ['./segmentpartdetails.page.scss'],
})
export class SegmentpartdetailsPage implements OnInit {

  type: string = 'Specification';
  oldPartNo:string;
  newPartNo:string;
  oemPartNo:string;
  engine:string;
  application:string;
  ApplicationPartDetailsJson=[];

  constructor(
    private modalCtrl: ModalController,
    private activateroute: ActivatedRoute,
    private config: ConfigService
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res) => {
      const obj = JSON.parse(res.segmentApplication);
      
      this.oldPartNo=obj.oldPartNo,
      this.newPartNo=obj.newPartNo,
      this.oemPartNo=obj.oemPartNo,
      this.engine=obj.engine,
      this.application=obj.application
    
    });
this.getSegmentPartDetails();
  }

  async getSegmentPartDetails() {

    const values = {
      "tca_oldpartno": this.oldPartNo,
      "tca_newpartno": this.newPartNo,
      "oem_partnumber":this.oemPartNo,
      "engine": this.engine,
      "application": this.application

    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/getSegmentApplicationParDetails', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();
        this.ApplicationPartDetailsJson = response.data;
      
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');

      }
      else {
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');

      }
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
    });

  }
  async openPreview() {
    const imgURL = "../../../../assets/CentralHousing.jpg";
    const modal = await this.modalCtrl.create({
      component: ImagemodalComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        img: imgURL
      }
    });
    modal.present();
  }

}
