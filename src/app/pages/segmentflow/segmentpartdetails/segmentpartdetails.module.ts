import { ComponentsModule } from './../../../component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SegmentpartdetailsPageRoutingModule } from './segmentpartdetails-routing.module';
import { SegmentpartdetailsPage } from './segmentpartdetails.page';
import { MatExpansionModule } from '@angular/material/expansion';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    MatExpansionModule,
    SegmentpartdetailsPageRoutingModule
  ],
  declarations: [SegmentpartdetailsPage]
})
export class SegmentpartdetailsPageModule { }
