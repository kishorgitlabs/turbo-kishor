import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SegmentapplicationPage } from './segmentapplication.page';

const routes: Routes = [
  {
    path: '',
    component: SegmentapplicationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SegmentapplicationPageRoutingModule {}
