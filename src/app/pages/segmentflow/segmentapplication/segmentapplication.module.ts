import { ComponentsModule } from './../../../component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SegmentapplicationPageRoutingModule } from './segmentapplication-routing.module';
import { SegmentapplicationPage } from './segmentapplication.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    SegmentapplicationPageRoutingModule
  ],
  declarations: [SegmentapplicationPage]
})
export class SegmentapplicationPageModule { }
