import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config/config.service';

@Component({
  selector: 'app-segmentapplication',
  templateUrl: './segmentapplication.page.html',
  styleUrls: ['./segmentapplication.page.scss'],
})
export class SegmentapplicationPage implements OnInit {


  segmentApplicationJson = [];
  makeName: string;
  segmentName: string;
  subSegmentName: string;

  constructor(
    private activateroute: ActivatedRoute,
    private config: ConfigService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activateroute.queryParams.subscribe((res) => {
      const obj = JSON.parse(res.segmentMakeObj);
      console.log(obj);
      this.segmentName = obj.segment;
      this.subSegmentName = obj.subsegment;
      this.makeName = obj.make;
    });
    this.getSegmentApplication();
  }


  async getSegmentApplication() {

    const values = {
      "oem_name": this.makeName,
      "segment": this.segmentName,
      "sub_segment": this.subSegmentName

    }
    this.config.loader('Loading...');
    this.config.postData('api/mobile/getSegmentApplication', values).subscribe((res) => {

      const response: any = res;

      if (response.result === true) {
        this.config.loaderDismiss();
        this.segmentApplicationJson = response.data;
      }
      else if (response.result === false) {
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');

      }
      else {
        this.config.loaderDismiss();
        this.config.msgAlertFn('No Data Found');

      }
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
    });

  }

  async gotoSegmentPartDetails(partDetails) {

    const values = {
      oldPartNo: partDetails.oldpartno,
      newPartNo: partDetails.newpartno,
      oemPartNo: partDetails.oempartno,
      engine: partDetails.engine,
      application: partDetails.application,

    }
    let navigation: NavigationExtras = {
      queryParams: {
        segmentApplication: JSON.stringify(values)
      }
    };

    this.router.navigate(['/segmentpartdetails'], navigation)
  }
}
