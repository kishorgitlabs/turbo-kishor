import { ComponentsModule } from './../../../component/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SegmentmakePageRoutingModule } from './segmentmake-routing.module';
import { SegmentmakePage } from './segmentmake.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    Ng2SearchPipeModule,
    SegmentmakePageRoutingModule
  ],
  declarations: [SegmentmakePage]
})
export class SegmentmakePageModule { }
