import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, LoadingController, MenuController, ModalController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { CustomalertsComponent } from 'src/app/component/customalerts/customalerts.component';
import { ForgotComponent } from 'src/app/component/forgot/forgot.component';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  public rootUrl = 'https://brainmagicllc.com/turboapi/';
  isLoading = false;
  constructor(
    private http: HttpClient,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController,
    private menuCtrl: MenuController,
    private modalCtrl:ModalController
  ) { }

  INRCurrency(x) {
    const currency = new Intl.NumberFormat('en-IN', { currency: 'INR' }).format(x);
    return currency;
  }

  menuFn() {
    this.menuCtrl.toggle();
  }

  companyName(code) {
    if (code === 'RML') {
      return "Rane Madras Limited";
    } else if (code === 'REVL') {
      return "Rane Engine Value Limited";
    } else if (code === 'RBL') {
      return "Rane Brake Lining Limited";
    } else if (code === 'TRW') {
      return "Rane Steering System Pvt. Ltd";
    } else if (code === 'RAP') {
      return "Rane Auto Parts";
    }
  }

  async companyAlert(msg){

    const modalCreate=await this.modalCtrl.create({
      component:CustomalertsComponent,
      cssClass:'alertclass',
      componentProps:{
        'alertName':msg
      }
    });
    return await modalCreate.present();
  }

  getData(url): Observable<any> {
    const geturl = `${this.rootUrl}${url}`;
    return this.http.get(geturl);
  }

  postData(url, bodyValues): Observable<any> {
    const apiURL = `${this.rootUrl}${url}`;
    return this.http.post(apiURL, bodyValues);
  }

  async loader(msg) {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      spinner: 'crescent',
      // message : `<div class='ion-text-center'><img src="assets/loading.gif"  /> <br/> <div class="pt-2" >${msg}</div></div>`,
      message: msg,
      //  message : `<img src="assets/loading.gif" class="img-align" /> <br/> <div class='ion-text-center'>${msg}</div> `,
      // spinner: spin,  // "bubbles" | "circles" | "circular" | "crescent" | "dots" | "lines" | "lines-small" | null | undefined
      // cssClass: 'my-custom-class',   // Write CSS in global.css
      // mode : 'ios',
      // duration: 20000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }
  async loaderDismiss() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  }



  async toastFn(msg: string, positiontxt: any = 'bottom', headerString?: string) {
    const toast = await this.toastCtrl.create({
      header: headerString,
      message: msg,
      cssClass: 'customToast',
      position: positiontxt,
      duration: 3000,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

  async msgAlertFn(msg) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        },
      ]
    });
    await alert.present();
  }

  async exitFunction() {
    const alert = await this.alertController.create({
      header: 'Exit Turbo App ?',
      message: 'Do you want to exit the app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Exit',
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });
    await alert.present();
  }

async forGotPassword(msg){

  const modalCreate =await this.modalCtrl.create({

    component:ForgotComponent,
    cssClass:'alertclass',
    componentProps:{
      'forgot':msg
    }
    
  });
  return await modalCreate.present();

}

}
