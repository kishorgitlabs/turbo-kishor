import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then(m => m.FolderPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'whatsnew',
    loadChildren: () => import('./pages/whatsnew/whatsnew.module').then(m => m.WhatsnewPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'make',
    loadChildren: () => import('./pages/manufacture/make/make.module').then(m => m.MakePageModule)
  },
  {
    path: 'makesegment',
    loadChildren: () => import('./pages/manufacture/makesegment/makesegment.module').then(m => m.MakesegmentPageModule)
  },
  {
    path: 'model',
    loadChildren: () => import('./pages/model/model.module').then(m => m.ModelPageModule)
  },
  {
    path: 'segment',
    loadChildren: () => import('./pages/segmentflow/segment/segment.module').then(m => m.SegmentPageModule)
  },

  {
    path: 'contact',
    loadChildren: () => import('./pages/contact/contact.module').then(m => m.ContactPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then(m => m.AboutPageModule)
  },
  {
    path: 'pricelist',
    loadChildren: () => import('./pages/pricelist/pricelist.module').then(m => m.PricelistPageModule)
  },
  {
    path: 'search/:mode',
    loadChildren: () => import('./pages/search/search.module').then(m => m.SearchPageModule)
  },
  {
    path: 'announcements',
    loadChildren: () => import('./pages/announcements/announcements.module').then(m => m.AnnouncementsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'feedback',
    loadChildren: () => import('./pages/feedback/feedback.module').then(m => m.FeedbackPageModule)
  },
  {
    path: 'makesubsegment',
    loadChildren: () => import('./pages/manufacture/makesubsegment/makesubsegment.module').then(m => m.MakesubsegmentPageModule)
  },
  {
    path: 'application',
    loadChildren: () => import('./pages/manufacture/application/application.module').then(m => m.ApplicationPageModule)
  },
  {
    path: 'partdetails',
    loadChildren: () => import('./pages/manufacture/partdetails/partdetails.module').then(m => m.PartdetailsPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./pages/faq/faq.module').then(m => m.FaqPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then(m => m.NotificationsPageModule)
  },
  {
    path: 'others',
    loadChildren: () => import('./pages/others/others.module').then(m => m.OthersPageModule)
  },
  {
    path: 'segmentsubsegment',
    loadChildren: () => import('./pages/segmentflow/segmentsubsegment/segmentsubsegment.module').then(m => m.SegmentsubsegmentPageModule)
  },
  {
    path: 'segmentmake',
    loadChildren: () => import('./pages/segmentflow/segmentmake/segmentmake.module').then(m => m.SegmentmakePageModule)
  },
  {
    path: 'segmentapplication',
    loadChildren: () => import('./pages/segmentflow/segmentapplication/segmentapplication.module').then(m => m.SegmentapplicationPageModule)
  },
  {
    path: 'segmentpartdetails',
    loadChildren: () => import('./pages/segmentflow/segmentpartdetails/segmentpartdetails.module').then(m => m.SegmentpartdetailsPageModule)
  },
  {
    path: 'applications',
    loadChildren: () => import('./pages/applicationflow/applications/applications.module').then(m => m.ApplicationsPageModule)
  },
  {
    path: 'network',
    loadChildren: () => import('./pages/network/network.module').then(m => m.NetworkPageModule)
  },
  {
    path: 'editprofile',
    loadChildren: () => import('./pages/editprofile/editprofile.module').then(m => m.EditprofilePageModule)
  },
  {
    path: 'searchbypartno',
    loadChildren: () => import('./pages/searchbypartno/searchbypartno.module').then( m => m.SearchbypartnoPageModule)
  },
  {
    path: 'enginespecs',
    loadChildren: () => import('./pages/applicationflow/enginespecs/enginespecs.module').then( m => m.EnginespecsPageModule)
  },
  {
    path: 'videos',
    loadChildren: () => import('./pages/videos/videos.module').then( m => m.VideosPageModule)
  },
  {
    path: 'pdf',
    loadChildren: () => import('./pages/pdf/pdf.module').then( m => m.PdfPageModule)
  },
  {
    path: 'videosdetails',
    loadChildren: () => import('./pages/videosdetails/videosdetails.module').then( m => m.VideosdetailsPageModule)
  },
  {
    path: 'pdfdetails',
    loadChildren: () => import('./pages/pdfdetails/pdfdetails.module').then( m => m.PdfdetailsPageModule)
  },
  {
    path: 'productcatlogue',
    loadChildren: () => import('./pages/productcatlogue/productcatlogue.module').then( m => m.ProductcatloguePageModule)
  },  {
    path: 'applicatonpartdetails',
    loadChildren: () => import('./pages/applicationflow/applicatonpartdetails/applicatonpartdetails.module').then( m => m.ApplicatonpartdetailsPageModule)
  },










];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
