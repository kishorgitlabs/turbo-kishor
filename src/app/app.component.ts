import { Component } from '@angular/core';
import { ConfigService } from './services/config/config.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Router } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/dashboard', icon: 'home' },
    { title: 'Manufacturer', url: '/make', icon: 'car-sport' },
    { title: 'Segment', url: '/segment', icon: 'bus' },
    { title: 'Application', url: '/applications', icon: 'document' },
    { title: 'Price List', url: '/pricelist', icon: 'pricetags' },
    { title: 'Search', url: '/search', icon: 'search' },
    { title: 'Network', url: '/network', icon: 'git-network' },
    { title: 'Settings', url: '/settings', icon: 'settings' }
  ];
  constructor(
    private screenOrientation: ScreenOrientation,
    private config: ConfigService,
    private network: Network,
    private route: Router,
    private statusBar: StatusBar,
    private platform: Platform,
  ) {
    // this.statusBar.overlaysWebView(false);
    // this.statusBar.backgroundColorByHexString('#ffffff');
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.network.onConnect().subscribe(() => {
        setTimeout(() => {
          if (this.network.type === 'wifi') {
            console.log('we got a wifi connection, woohoo!');
          }
        }, 3000);
      });

      this.network.onDisconnect().subscribe(() => {
 
        this.config.companyAlert('No Internet Connection')
 
        // this.config.toastFn(`Internet is not available  ☹️`);
      });
    });

  }

  logoutFn() {
    localStorage.clear();
    this.route.navigate(['register']);
  }
}
